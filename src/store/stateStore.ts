import { create } from 'zustand';

export interface StoreState {
  isOpenCart: boolean;
  isOpenMenu: boolean;
  isOpenCallBackModal: boolean;
  toggleIsOpenCart: () => void;
  toggleIsOpenMenu: () => void;
  toggleIsOpenCallBackModal: () => void;
}

type StoreStateCreator = (
  set: (partial: Partial<StoreState> | ((state: StoreState) => Partial<StoreState>), replace?: boolean) => void,
  get: () => StoreState,
  api: any
) => StoreState;

const createStoreState: StoreStateCreator = (set, get) => ({
  isOpenCart: false,
  isOpenMenu: false,
  isOpenCallBackModal: false,
  toggleIsOpenCart: () => {
    set(state => ({ isOpenCart: !state.isOpenCart }));
  },
  toggleIsOpenMenu: () => {
    set(state => ({ isOpenMenu: !state.isOpenMenu }));
  },
  toggleIsOpenCallBackModal: () => {
    set(state => ({ isOpenCallBackModal: !state.isOpenCallBackModal }));
  },
});

const useStateStore = create(createStoreState);

export default useStateStore;
