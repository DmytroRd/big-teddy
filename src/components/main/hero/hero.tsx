import Link from 'next/link';
import s from './hero.module.scss';

const Hero = () => {
  return (
    <div className={s.hero}>
      <div className={s.content}>
        <div className={s.text}>
          <p>
            Від дитячих днів народження до романтичних жестів, мякий плюшевий ведмедик зробить кожну мить особливою!
          </p>
        </div>

        <Link className={s.link} href="/">
          <span>Обрати ведмедика</span>
        </Link>
      </div>
    </div>
  );
};
export default Hero;
//          <p>В нашому магазині ми пропонуємо найм`якших і найгарніших плющевих ведмедиків.</p>
