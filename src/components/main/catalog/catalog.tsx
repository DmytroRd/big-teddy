import Container from '@/components/ui/container/container';
import ProductCard from '@/components/product-card/productCard';
import data from '@/data/products.json';
import s from './catalog.module.scss';
export default function Catalog() {
  return (
    <Container>
      <div className={s.catalog}>
        <div className={s.products}>
          {data.map(product => (
            <ProductCard key={product.id} product={product} />
          ))}
        </div>
      </div>
    </Container>
  );
}
