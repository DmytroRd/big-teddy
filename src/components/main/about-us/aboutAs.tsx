import DropDown from './dropdown';
import { aboutAsData } from '@/data/about_as';
import s from './aboutUs.module.scss';

const AboutAs = () => {
  return (
    <section className={`${s.aboutAs} container`}>
      {aboutAsData && aboutAsData.map((item, i) => <DropDown key={i} title={item.title} text={item.text} />)}
    </section>
  );
};
export default AboutAs;
