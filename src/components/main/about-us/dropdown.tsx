'use client';
import { useState } from 'react';
import { Icons } from '@/components/ui/icons/icons';
import s from './aboutUs.module.scss';

interface DropDownProps {
  title: string;
  text: string[];
}

const DropDown = ({ title, text }: DropDownProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <div className={s.block}>
      <button onClick={()=> setIsOpen(!isOpen)} className={`${s.title} ${isOpen ? s.open : ''}`}>{title}<Icons.chevronDown/></button>
      <div className={`${s.description} ${isOpen ? s.open : ''}`}>
        <div className={s.content}>
          {text && Array.isArray(text) && text.length > 0 && text.map((item, i) => <p key={i}>{item}</p>)}
        </div>
      </div>
    </div>
  );
};

export default DropDown;
