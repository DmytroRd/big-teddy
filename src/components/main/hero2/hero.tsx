import Link from 'next/link';
import s from './hero.module.scss';

const Hero2 = () => {
  return (
    <div className={s.hero}>
      <div className={`${s.content} container`}>
        <div className={s.text}>
          <p>
            Від дитячих днів народження до романтичних жестів - мякий плюшевий ведмедик зробить кожну мить особливою!
          </p>
        </div>
        <div className={s.image}>
        </div>
      </div>
    </div>
  );
};
export default Hero2;

