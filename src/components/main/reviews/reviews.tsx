import { Icons } from '@/components/ui/icons/icons';
import s from './reviews.module.scss';

const Reviews = () => {
  return (
    <section className={`${s.reviews} container`}>
      <h3 className={`${s.title} mainTitle`}>
        <span className='icon'>
          <Icons.comment size="extralarge" />
        </span>
        Відгуки
      </h3>
      <div className={`${s.content}`}>
        <div className={s.swiperBlock}></div>
      </div>
    </section>
  );
};
export default Reviews;
