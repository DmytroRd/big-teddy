'use client';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/navigation';
import './swiperImages.scss';

const SwiperImages = ({ data }: any) => {
  const [images, setImages] = useState(data.images);
  return (
    <div className="imagesContainer">
      <Swiper className="swiper-container " spaceBetween={24}>
        {images &&
          Array.isArray(images) &&
          images.length > 0 &&
          images.map((item, i) => (
            <SwiperSlide key={i} className="swiper-slide">
              <Image src={item} alt="Best Teddy" fill sizes="100%" />
            </SwiperSlide>
          ))}
      </Swiper>
      <Swiper className="swiper-list" spaceBetween={8} slidesPerView={'auto'}>
        {images &&
          Array.isArray(images) &&
          images.length > 0 &&
          images.map((item, i) => (
            <SwiperSlide key={i} className="swiper-list-slide">
              <Image src={item} alt="Best Teddy" fill sizes="100%" />
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
};
export default SwiperImages;
