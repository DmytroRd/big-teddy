import { title } from 'process';
import s from './ourBears.module.scss';
import BearSection from './bear-section/bearSection';
import { Icons } from '@/components/ui/icons/icons';

//капучіно rgb(134, 118, 118)
//коричневий  rgb(162, 73, 36);
// білий rgb(255, 255, 255);
// сірий rgb(156, 156, 156);
// рожевий rgb(244, 100, 164);
// червоний rgb(255, 0, 0);
// бежевий #eadac3;
// чорний   rgb(0, 0, 0);
//
//

const bearsData = [
  {
    id: 1,
    title: 'Tommy',
    images: [
      '/tommy/tommy-200-bej.jpg',
      '/tommy/tommy-200-capuch.jpg',
      '/tommy/tommy-200-grey.webp',
      '/tommy/tommy-200-red.webp',
    ],
    description: [
      'Плюшевий ведмедик Tommy є нашим улюбленцем, а також фаворитом серед наших клієнтів. Його пишні форми та неймовірно ніжне штучне хутро найкраще підходять для обіймів',
      ' Ведмедик Томик виготовлений з екологічно чистих гіпоалергенних, безпечних та високоякісних матеріалів, що гарантують його міцність і довговічність.',
      ' Ведмедик Томик буде прекрасним подарунком як для дітей так і дорослим. Він подарує неймовірні емоції своєму новому власнику, стане прекрасним компаньйоном та другом, а також наповнить інтерєр оселі затишком та гарним настроєм.',
      'Великий плюшевий Ведмедик Томмі - це найкращий подарунок, який дивує та дарує неймовірні емоції. Це чудовий варіант подарунку як для дітей так і дорослих.',
    ],
    colors: [
      { id: 1, title: 'Бежевий', color: '#eadac3' },
      { id: 2, title: 'Білий', color: 'rgb(255, 255, 255)' },
      { id: 3, title: 'Капучино', color: 'rgb(134, 118, 118)' },
      { id: 4, title: 'Сірий', color: 'rgb(156, 156, 156)' },
      { id: 5, title: 'Рожевий', color: 'rgb(244, 100, 164)' },
      { id: 6, title: 'Червоний', color: 'rgb(255, 0, 0)' },
      { id: 7, title: 'Чорний', color: 'rgb(0, 0, 0)' },
      { id: 8, title: 'Коричневий', color: 'rgb(162, 73, 36)' },
    ],
    sizes: [
      { id: 1, title: '80', price: 650 },
      { id: 2, title: '100', price: 850 },
      { id: 3, title: '160', price: 1150 },
      { id: 4, title: '200', price: 1550 },
    ],
  },
  {
    id: 2,
    title: 'Monty',
    images: [
      '/tommy/tommy-200-bej.jpg',
      '/tommy/tommy-200-capuch.jpg',
      '/tommy/tommy-200-grey.webp',
      '/tommy/tommy-200-red.webp',
    ],
    description: [
      'Плюшевий ведмедик Tommy є нашим улюбленцем, а також фаворитом серед наших клієнтів',
      ' Ведмедик Томик виготовлений з високоякісних матеріалів, що гарантують його міцність і довговічність.',
      '  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quia esse nulla vero vel voluptas pariatur, ad assumenda repudiandae laudantium autem harum, vitae ipsum unde quae animi voluptatum eligendi asperiores libero! At optio a dolorem id nam quod, excepturi commodi quos repudiandae sequi consequuntur sed quas pariatur minima facere dolorum repellendus autem veniam porro recusandae accusamus molestiae aperiam inventore. Nesciunt reprehenderit minima deserunt, placeat dolor possimus quibusdam corporis voluptate odit, dolore sed maiores eveniet repellat optio debitis dolorum iste consequatur ex facilis praesentium, voluptates est animi hic odio. Soluta natus tempora laboriosam, commodi placeat ea reprehenderit quaerat inventore necessitatibus, at vero tempore alias amet! Aut,quos. Minima provident ratione et sequi qui id aspernatur blanditiis, sapiente maxime itaque repudiandae possimus? Tenetur.',
    ],
    colors: [
      { id: 1, title: 'Бежевий', color: '#eadac3' },
      { id: 2, title: 'Білий', color: 'rgb(255, 255, 255)' },
      { id: 3, title: 'Капучино', color: 'rgb(134, 118, 118)' },
      { id: 4, title: 'Сірий', color: 'rgb(156, 156, 156)' },
      { id: 5, title: 'Рожевий', color: 'rgb(244, 100, 164)' },
      { id: 6, title: 'Червоний', color: 'rgb(255, 0, 0)' },
      { id: 7, title: 'Чорний', color: 'rgb(0, 0, 0)' },
      { id: 8, title: 'Коричневий', color: 'rgb(162, 73, 36)' },
    ],
    sizes: [
      { id: 1, title: '80', price: 650 },
      { id: 2, title: '100', price: 850 },
      { id: 3, title: '160', price: 1150 },
      { id: 4, title: '200', price: 1550 },
    ],
  },
  {
    id: 3,
    title: 'Vetly',
    images: [
      '/tommy/tommy-200-bej.jpg',
      '/tommy/tommy-200-capuch.jpg',
      '/tommy/tommy-200-grey.webp',
      '/tommy/tommy-200-red.webp',
    ],
    description: [
      'Плюшевий ведмедик Tommy є нашим улюбленцем, а також фаворитом серед наших клієнтів',
      ' Ведмедик Томик виготовлений з високоякісних матеріалів, що гарантують його міцність і довговічність.',
      '  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quia esse nulla vero vel voluptas pariatur, ad assumenda repudiandae laudantium autem harum, vitae ipsum unde quae animi voluptatum eligendi asperiores libero! At optio a dolorem id nam quod, excepturi commodi quos repudiandae sequi consequuntur sed quas pariatur minima facere dolorum repellendus autem veniam porro recusandae accusamus molestiae aperiam inventore. Nesciunt reprehenderit minima deserunt, placeat dolor possimus quibusdam corporis voluptate odit, dolore sed maiores eveniet repellat optio debitis dolorum iste consequatur ex facilis praesentium, voluptates est animi hic odio. Soluta natus tempora laboriosam, commodi placeat ea reprehenderit quaerat inventore necessitatibus, at vero tempore alias amet! Aut,quos. Minima provident ratione et sequi qui id aspernatur blanditiis, sapiente maxime itaque repudiandae possimus? Tenetur.',
    ],
    colors: [
      { id: 1, title: 'Бежевий', color: '#eadac3' },
      { id: 2, title: 'Білий', color: 'rgb(255, 255, 255)' },
      { id: 3, title: 'Капучино', color: 'rgb(134, 118, 118)' },
      { id: 4, title: 'Сірий', color: 'rgb(156, 156, 156)' },
      { id: 5, title: 'Рожевий', color: 'rgb(244, 100, 164)' },
      { id: 6, title: 'Червоний', color: 'rgb(255, 0, 0)' },
      { id: 7, title: 'Чорний', color: 'rgb(0, 0, 0)' },
      { id: 8, title: 'Коричневий', color: 'rgb(162, 73, 36)' },
    ],
    sizes: [
      { id: 1, title: '80', price: 650 },
      { id: 2, title: '100', price: 850 },
      { id: 3, title: '160', price: 1150 },
      { id: 4, title: '200', price: 1550 },
    ],
  },
];

const OurBears = () => {
  return (
    <section className={`${s.ourBears}`}>
      <h2 className="mainTitle container">
        <span className="icon">
          <Icons.bear size="extralarge" />
        </span>
        Наші <span>ведмедики</span>
      </h2>
      <div className={`${s.bears}`}>
        <div className={`${s.content} container`}>
          {bearsData &&
            Array.isArray(bearsData) &&
            bearsData.length > 0 &&
            bearsData.map(item => <BearSection key={item.id} item={item} />)}
        </div>
      </div>
    </section>
  );
};

export default OurBears;
0;
