'use client';
import Link from 'next/link';
import s from './callback-list.module.scss';

const CallbackList = () =>  {
  return (
    <ul className={s.list}>
      <li>
        <Link className={s.instagram} href="/">
          Instagram
        </Link>
      </li>
      <li>
        <Link className={s.viber} href="/">
          Viber
        </Link>
      </li>
      <li>
        <Link className={s.telegram} href="/">
          Telegramm
        </Link>
      </li>
      <li>
        <Link className={s.phone} href="/">
          097 845 5001
        </Link>
      </li>
    </ul>
  );
}
export default CallbackList;