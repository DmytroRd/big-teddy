'use client';
import { useState } from 'react';
import CallbackList from './callback-list/callback-list';
import Button from '@/components/ui/button/button';
import { Icons } from '@/components/ui/icons/icons';
import s from './callback.module.scss';

const CallbackDropdown = () => {
  const [showBlock, setShowBlock] = useState(false);
  return (
    <div
      className={s.callbackContainer}
      onMouseEnter={() => setShowBlock(true)}
      onMouseLeave={() => setShowBlock(false)}
    >
      <button className={s.trigger}>
        <Icons.phone size="small" />
        Зв`язатися з нами
        <span className={showBlock ? s.rotateIcon : ''}>
          <Icons.chevronDown size="medium" />
        </span>
      </button>

      {showBlock && (
        <div className={s.block}>
          <CallbackList />
          <div className={s.info}>
            <Button color="brown">
              <Icons.phone />
              Зворотній дзвінок
            </Button>
            <p>Працюємо щоденно з 9:00 до 19:00</p>
          </div>
        </div>
      )}
    </div>
  );
};
export default CallbackDropdown;
