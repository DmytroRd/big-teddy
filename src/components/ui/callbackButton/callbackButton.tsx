'use client';
import { Icons } from '../icons/icons';
import s from './callbackButton.module.scss';

const CallbackButton = () => {
  return (
    <button className={s.callbackButton}>
      <Icons.phone size="small" />
      Зворотній дзвінок
    </button>
  );
};
export default CallbackButton;
