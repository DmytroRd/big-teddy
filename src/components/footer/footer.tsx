import Button from '../ui/button/button';
import { Icons } from '../ui/icons/icons';
import Link from 'next/link';
import s from './footer.module.scss';
import CallbackButton from '../ui/callbackButton/callbackButton';

const Footer = () => {
  return (
    <footer className={s.footer}>
      <div className={`${s.content} container`}>
        <div className={s.contentBlock}>
          <div className={s.footerLogo}>
            <Link href="/" className={s.logo}></Link>
            <div className={s.text}>
              <span className={s.title}>Big Teddy </span>
              <p className={s.info}>Найкращі плюшеві ведмедики для вас та ваших найрідніших!</p>
            </div>
          </div>
          <div className={s.infoBlocks}>
            <div className={s.block}>
              <span className={s.title}> Графік работы: </span>

              <div className={s.info}>
                <span>Пн-Нд - 9:00-19:00</span>
                <span>без вихідних</span>
              </div>
            </div>
            <div className={s.block}>
              <span className={s.title}>Наші контакти: </span>
              <div className={s.info}>
                <div className={s.socials}>
                  <Link className={s.instagram} href="/"></Link>
                  <Link className={s.viber} href="/"></Link>
                  <Link className={s.telegram} href="/"></Link>
                </div>
                <Link className={s.phone} href="/">
                  big_teddy@ukr.net
                </Link>
                <Link className={s.phone} href="/">
                  097 845 5001
                </Link>
                <CallbackButton />
              </div>
            </div>
          </div>
        </div>
        <div className={s.privacyBlock}>
          <p>© 2024 Всі права захищені</p>
          <button>Політика конфіденційності</button>
          <button>Всі права захищені</button>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
