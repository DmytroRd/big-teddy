'use client';
import { useRef } from 'react';
import Link from 'next/link';
import { CSSTransition } from 'react-transition-group';
import { useClickOutside } from '@/utils/useClickOutside';
import { useBodyScrollLock } from '@/utils/useBodyScrollLock';
import useStateStore from '@/store/stateStore';
import CallbackList from '../ui/callback/callback-list/callback-list';
import Button from '@/components/ui/button/button';
import { Icons } from '@/components/ui/icons/icons';

import s from './mob-menu.module.scss';

const MobMenu = () => {
  const { isOpenMenu, toggleIsOpenMenu } = useStateStore();
  const mobMenu = useRef<HTMLDivElement>(null);
  useBodyScrollLock(isOpenMenu);
  useClickOutside(isOpenMenu, mobMenu, () => {
    toggleIsOpenMenu();
  });

  return (
    <CSSTransition
      in={isOpenMenu}
      nodeRef={mobMenu}
      timeout={500}
      classNames={{
        enter: s.cartEnter,
        enterActive: s.cartEnterActive,
        exit: s.cartExit,
        exitActive: s.cartExitActive,
      }}
      unmountOnExit
    >
      <div className={`${s.wrapper} ${isOpenMenu && s.visible}`}>
        <div className={s.content} ref={mobMenu}>
          <div className={s.mobMenu}>
            <div className={s.title}>
              <div onClick={toggleIsOpenMenu}>Logo</div>
              <button className={s.close} onClick={toggleIsOpenMenu}>
                <Icons.close size="medium" />
              </button>
            </div>

            <Link className={s.user} href="/login">
              <Icons.person size="medium" />
              <span>Вхід до кабінету</span>
            </Link>
            <div className={s.callback}>
              <CallbackList />
            </div>

            <div className={s.info}>
              <Button width="full" color="green">
                <Icons.phone />
                Зворотній дзвінок
              </Button>
              <p>Працюємо щоденно з 9:00 до 19:00</p>
            </div>
          </div>
        </div>
      </div>
    </CSSTransition>
  );
};

export default MobMenu;
