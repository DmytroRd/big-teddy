import CallbackDropdown from '@/components/ui/callback/callback';
import s from '../header.module.scss';

const Header1 = () => {
  return (
    <div className={s.header1}>
      <div className={`${s.content} container`}>
        <CallbackDropdown />
      </div>
    </div>
  );
};

export default Header1;
