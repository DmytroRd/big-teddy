'use client';
import Link from 'next/link';
import { Icons } from '@/components/ui/icons/icons';
import useScroll from '@/utils/useScroll';
import s from './header2.module.scss';
import ButtonIcon from '@/components/ui/button-icon/button-icon';
import useCartStore from '@/store/cartStore';
import useStateStore from '@/store/stateStore';
interface NavItem {
  title: string;
  id: string;
  url: string;
}

const navList: NavItem[] = [
  { title: 'Каталог', id: '1', url: '/' },
  { title: 'Про нас', id: '2', url: '/' },
  { title: 'Відгуки', id: '3', url: '/' },
  { title: 'Оплата та доставка', id: '4', url: '/' },
  { title: 'Контакти', id: '5', url: '/' },
];

const Header2 = () => {
  const isScrolled = useScroll({ offset: 20 });
  const { toggleCartVisibility } = useCartStore();
  const { toggleIsOpenMenu } = useStateStore();
  return (
    <div className={`${s.header2} ${isScrolled ? s.fixedHeader : ''}`}>
      <div className={`${s.content} container`}>
        <button className={s.mobMenuButton} onClick={toggleIsOpenMenu}>
          <Icons.menu />
          Меню
        </button>
        <Link href="/" className={s.logo}></Link>
        <nav className={s.nav}>
          {navList.map(item => (
            <Link key={item.id} href={item.url} className={s.listItem}>
              {item.title}
            </Link>
          ))}
        </nav>

        <button className={s.cartButton} onClick={toggleCartVisibility}>
          <Icons.cartOutlined size="medium" />
          <span className={s.name}>Кошик</span>
          <span className={s.count}>({0})</span>
        </button>
      </div>
    </div>
  );
};

export default Header2;

/*
  <div className={s.buttonsIcons}>
          <ButtonIcon count="0">
            <Icons.favoriteOutlined size="medium" />
          </ButtonIcon>
          <ButtonIcon onClick={toggleCartVisibility} count="0">
            <Icons.cartOutlined size="medium" />
          </ButtonIcon>
        </div>
*/
