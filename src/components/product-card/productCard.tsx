'use client';
import React, { useState, useEffect } from 'react';
import s from './productCard.module.scss';
import { Icons } from '@/components/ui/icons/icons';
import Button from '@/components/ui/button/button';
import Image from 'next/image';

interface ProductCardProps {
  product: {
    name: string;
    type: string;
    height: string;
    id: string;
    color: string;
    price: string;
    count: string;
    code: string;
    url: string;
  };
}

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  const { name, type, height, id, color, price, count, code, url } = product;
  const [isFavorite, setIsFavorite] = useState(false);
  // const [hover, setHover] = useState(false); onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}
  return (
    <div className={s.productCard}>
      <Image src={url} alt={name} width={200} height={200} />
      <div className={s.cardDescription}>
        <h3>
          <p className={s.name}>{type} <span>{name}</span></p>
          <p className={s.height}>{height}<span>см</span></p>
          <p className={s.color}>{color}</p>
        </h3>
        <div className={s.priceblock}>
          <p className={s.price}>
            {price}
            <span> грн</span>
          </p>
          <Button>
            <Icons.buy size="medium" />
            Купити
          </Button>
        </div>
      </div>
      <button className={s.favorite}>
        {isFavorite ? <Icons.favoriteFilled size="small" /> : <Icons.favoriteOutlined size="small" />}
      </button>
    </div>
  );
};

export default ProductCard;
