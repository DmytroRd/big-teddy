import { useEffect } from 'react';

export const useBodyScrollLock = (isLocked: boolean) => {
  useEffect(() => {
    const htmlElement = document.documentElement;
    if (isLocked) {
      htmlElement.style.overflow = 'hidden';
    } else {
      htmlElement.style.overflow = 'auto';
    }
    return () => {
      htmlElement.style.overflow = 'auto';
    };
  }, [isLocked]);
};

//відміна scroll для body
export const useBodyScrollLockRight = (isActive: boolean) => {
  useEffect(() => {
    if (isActive) {
      const paddingForScroll = window.innerWidth - document.body.offsetWidth + 'px';
      document.body.style.overflow = 'hidden';
      document.body.style.paddingRight = paddingForScroll;
      return () => {
        document.body.style.overflow = 'auto';
        document.body.style.paddingRight = '0';
      };
    }
  }, [isActive]);
};

export const useBodyScrollLockLeft = (isActive: boolean) => {
  useEffect(() => {
    if (isActive) {
      const paddingForScroll = window.innerWidth - document.body.offsetWidth + 'px';
      document.body.style.overflow = 'hidden';
      document.body.style.paddingLeft = paddingForScroll;
      return () => {
        document.body.style.overflow = 'auto';
        document.body.style.paddingLeft = '0';
      };
    }
  }, [isActive]);
};
