'use client'
import { useEffect, useState } from 'react';

interface ScrollOptions {
  offset?: number;
}

const useScroll = ({ offset = 0 }: ScrollOptions = {}) => {
  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const scrollY = window.scrollY;
      setIsScrolled(scrollY > offset);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [offset]);

  return isScrolled;
};

export default useScroll;
