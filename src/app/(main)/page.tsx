import Info from '@/components/main/info/info';
import OurBears from '@/components/main/our-bears/ourBears';
import AboutAs from '@/components/main/about-us/aboutAs';
import Reviews from '@/components/main/reviews/reviews';
import Hero from '@/components/main/hero/hero';
const Home = async () => {
  return (
    <>
      <Hero />
      <Info />
      <OurBears />
      <AboutAs />
      <Reviews />
    </>
  );
};
export default Home;
//  <Greeting />   <Hero /> <Catalog />    <Hero2 />
