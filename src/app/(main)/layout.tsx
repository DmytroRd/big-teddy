import Header from '@/components/header/header';
import Footer from '@/components/footer/footer';
import Cart from '@/components/cart/cart';
import MobMenu from '@/components/mob-menu/mob-menu';
export default async function MainLayout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Header />
      <main>{children}</main>
      <Footer />
      <Cart />
      <MobMenu />
    </>
  );
}
