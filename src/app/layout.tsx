import type { Metadata } from 'next';
import { roboto } from '@/fonts';
import Head from 'next/head';
import '../styles/globals.scss';


export const metadata: Metadata = {
  title: 'Teddy bears',
  description: 'Великі плюшеві ведмедики від Українського виробника Teddy',
};

export default async function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="keywords" content="teddy bears, plush, toys" />
        <link rel="icon" href="/favicon.ico" sizes="any" />
      </Head>
      <body className={roboto.className}>{children}</body>
    </html>
  );
}
